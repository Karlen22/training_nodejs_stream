const moment = require('moment');
const fs = require('fs');
const { Readable, Writable, Transform } = require('stream');

const file =  fs.createWriteStream('./file.txt');

class ReadStream extends Readable {
    _read() {
        setTimeout(() => {
            const buf = Buffer.from(`${new Date()}`);
            this.push(buf)
        }, 1000)
    }
}

class WriteStream extends Writable {
    _write(chunk, encoding, callback) {
      file.write(`${chunk.toString()}\n`);
      callback()
    }
}

class TransformStream extends Transform {
    _transform(chunk, encoding, callback) {
      try {
        const date = new Date(chunk.toString());
        const currentTime = moment(date).format("HH:mm:ss")

        callback(null, currentTime)
      } catch (err) {
        callback(err)
      }
    }
}

const readStream = new ReadStream();
const writeStream = new WriteStream();
const transformStream = new TransformStream();


readStream
  .pipe(transformStream)
  .pipe(writeStream)
